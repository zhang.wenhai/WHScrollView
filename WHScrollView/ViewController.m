//
//  ViewController.m
//  WHScrollView
//
//  Created by apple on 8/3/15.
//  Copyright (c) 2015 zhang.wenhai. All rights reserved.
//

#import "ViewController.h"
//导入头文件
#import "WHScrollView.h"

//遵守代理
@interface ViewController () <UIScrollViewDelegate>


@property (nonatomic, strong) WHScrollView *scrollView;

- (IBAction)btnOnClicked:(UIButton *)sender;
@property (nonatomic, strong) NSString *text;

@end

@implementation ViewController

- (void)viewDidLoad {
	[super viewDidLoad];
	self.view.backgroundColor = [UIColor lightGrayColor];
}


- (IBAction)btnOnClicked:(UIButton *)sender {
	
	//创建一个WHScrollView
	WHScrollView *scrollView = [[WHScrollView alloc] init];
	//设置代理
	scrollView.delegate = self;
	//创建需要显示的内容View，想要显示的信息全部写在这个view上
	UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 1200)];
	//设置将显示信息的view
	scrollView.viewOfContent = view;
	//防止销毁保存了下，在代理方法中用
	self.scrollView = scrollView;
	
	//覆盖navigationBar上面，所以添加到self.navigationController.view上
	[self.navigationController.view addSubview:scrollView];
	//设置显示
	[scrollView showWHScrollView];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
	
	//设置滚动效果
	[self.scrollView setScrollViewDidScrollAction];
}

@end
