//
//  main.m
//  WHScrollView
//
//  Created by apple on 8/3/15.
//  Copyright (c) 2015 zhang.wenhai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
	@autoreleasepool {
	    return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
	}
}
