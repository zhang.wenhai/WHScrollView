Pod::Spec.new do |s|
  s.name         = 'WHScrollView'
  s.version      = '1.0'
  s.summary      = '点击新闻内容Cell时跳转到内容页效果'
  s.author       = { 'zhang.wenhai' => 'zhang_mr1989@163.com' }
  s.source_files  = 'WHScrollView/WHScrollView.{h,m}'
  s.homepage         = 'https://github.com/zhang33121/WHScrollView'
  s.source           = { :git => 'https://github.com/zhang33121/WHScrollView.git', :tag => '1.0' }
  s.requires_arc     = true
end
