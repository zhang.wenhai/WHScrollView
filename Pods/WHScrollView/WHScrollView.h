//
//  WHScrollView.h
//  WHScrollView
//
//  Created by apple on 8/3/15.
//  Copyright (c) 2015 zhang.wenhai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WHScrollView : UIScrollView

@property (nonatomic, assign) NSInteger contentSizeOfHeight;

@property (nonatomic, strong) UIView *viewOfContent;

- (void)showWHScrollView;

- (void)setScrollViewDidScrollAction;

@end
