//
//  WHScrollView.m
//  WHScrollView
//
//  Created by apple on 8/3/15.
//  Copyright (c) 2015 zhang.wenhai. All rights reserved.
//

#import "WHScrollView.h"

#define SCREENW							[UIScreen mainScreen].bounds.size.width
#define SCREENH							[UIScreen mainScreen].bounds.size.height
#define kDurationOfShowWHScrollView		0.35
#define kContentViewBackgroundColor		[UIColor purpleColor]


@implementation WHScrollView

- (void)setViewOfContent:(UIView *)viewOfContent {
	
	_viewOfContent = viewOfContent;
	
	//修改所需滚动范围
	self.contentSize = CGSizeMake(SCREENW, self.viewOfContent.frame.size.height);
	
	UIView *rightView = [[UIView alloc] init];
	//如果内容View的高度小于屏幕的高度，那么创建屏幕大小的背景View
	if (viewOfContent.frame.size.height < SCREENW) {
		rightView.frame = CGRectMake(0, 0, SCREENW, SCREENH);
	}
	//如果内容View的高度大于屏幕的高度，那么创建背景View的高位为内容View的高度
	else {
		rightView.frame = CGRectMake(0, 0, SCREENW, viewOfContent.frame.size.height);
	}
	//设置背景View的颜色
	rightView.backgroundColor = kContentViewBackgroundColor;
	//将内容View添加到背景View上
	[rightView addSubview:viewOfContent];
	//将背景View添加到本ScrollView上
	[self addSubview:rightView];
}

- (instancetype)init {
	if (self = [super init]) {
		self.frame = CGRectMake(SCREENW, 0, SCREENW, SCREENH);
		[self setScrollViewConfig];
	}
	return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
	if (self = [super initWithFrame:frame]) {
	}
	return self;
}


- (void)setScrollViewConfig {
	
	//设置默认滚动范围
	self.contentSize = CGSizeMake(SCREENW, SCREENH);
	//弹簧效果：否
	self.bounces = NO;
	//锁定方向
	self.directionalLockEnabled = YES;
	//设置左侧冗余边缘为屏幕宽度
	self.contentInset = UIEdgeInsetsMake(0, SCREENW, 0, 0);
	//设置翻页支持
	self.pagingEnabled = YES;
	//
	self.showsHorizontalScrollIndicator = NO;
	//配置scrollView颜色
	self.backgroundColor = [UIColor clearColor];
}

- (void)setScrollViewDidScrollAction {
	
	//计算在哪页上
	int pageN = self.contentOffset.x / self.frame.size.width;
	//如果到-1页了，那么从父控件中删除本控件
	if (pageN == -1) {
		[self removeFromSuperview];
	}
	//如果X方向在移动，设置翻页支持，关闭纵向滚轮
	if (self.contentOffset.x < 0) {
		self.pagingEnabled = YES;
		self.showsVerticalScrollIndicator = NO;
	}
	else {
		self.pagingEnabled = NO;
		self.showsVerticalScrollIndicator = YES;
	}
}


- (void)showWHScrollView {
	
	self.frame = CGRectMake(SCREENW, 0, SCREENW, SCREENH);
	[UIView animateWithDuration:kDurationOfShowWHScrollView animations:^{
		self.frame = CGRectMake(0, 0, SCREENW, SCREENH);
	}];
}




@end
